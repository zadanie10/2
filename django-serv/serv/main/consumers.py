
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'serv.settings')
import django
django.setup()

from channels.generic.websocket import AsyncWebsocketConsumer
import json
from .models import Messages

from django.contrib.auth.models import User

from asgiref.sync import sync_to_async

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.chat_id = self.scope["url_route"]["kwargs"]["chat_id"]
        self.chat_group_name = "room_%s" % self.chat_id
        await self.channel_layer.group_add(self.chat_group_name, self.channel_name)
        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.chat_group_name, self.channel_name)

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]
        sender = text_data_json["sender"]
        try:
            user = await sync_to_async(User.objects.get, thread_sensitive=True)(pk=int(sender))
            msg = await sync_to_async(Messages.objects.create, thread_sensitive=True)(sender=user,text=message)
            await self.channel_layer.group_send(
                self.chat_group_name, {"type": "chat_message", "message": message,"sender_name":user.username,"time":msg.time}
            )
        except User.DoesNotExist:
            pass
        
    async def chat_message(self, event):
        message = event["message"]

        # Send message to WebSocket
        await self.send(text_data=json.dumps({"message": message}))
