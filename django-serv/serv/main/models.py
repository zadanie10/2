from django.db import models
from django.contrib.auth.models import User


class Chat(models.Model):
    title = models.CharField(max_length=200,verbose_name="Chat title")
    public_id = models.IntegerField(unique=True)

class Messages(models.Model):
    sender = models.ForeignKey(User,on_delete=models.CASCADE,related_name='messages')
    chat = models.ForeignKey(Chat,on_delete=models.CASCADE,related_name='messages')
    text = models.TextField()
    time = models.DateTimeField(auto_now_add=True)