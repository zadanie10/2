from django.http import HttpResponse
from django.shortcuts import render
from .forms import LoginForm
from django.contrib.auth import authenticate, login
from asgiref.sync import sync_to_async
from .models import Chat,Messages

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'], password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    elif request.method == 'GET':
        form = LoginForm()
        return render(request, 'account/login.html', {'form': form})

def chat(request,chat_id):
    if request.user.is_authenticated:
        try:
            chat = Chat.objects.get(public_id=chat_id)
        except:
            return HttpResponse("<h1>Chat not found</h1><a href='/chats'>All Chats</a>")
        return render(request, "chat/room.html", {"chat_id": chat_id,"chat_title":chat.title})
    else:
        return HttpResponse("<a href='/login'>Login please at first</a>")

# def index(request):
#     if request.user.is_authenticated:
        